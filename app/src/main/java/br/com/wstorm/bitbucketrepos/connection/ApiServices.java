package br.com.wstorm.bitbucketrepos.connection;

import java.util.List;

import br.com.wstorm.bitbucketrepos.model.ApiResponseRepos;
import br.com.wstorm.bitbucketrepos.model.PullRequest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by wevertonperon on 30/05/16.
 */
public interface ApiServices {

    @GET
    Call<ApiResponseRepos> getRepos(@Url String url);

    @GET
    Call<List<PullRequest>> getPullRequests(@Url String url);


}
