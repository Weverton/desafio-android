package br.com.wstorm.bitbucketrepos.util;

/**
 * Created by wevertonperon on 30/05/16.
 */
public class Constants {

    public static final String URL_BASE = "https://api.github.com/";

    public static final String URL_REPOS = "search/repositories?q=language:Java&sort=stars&page=%s";

    public static final String URL_DETAILS = "repos/%s/%s/pulls";

}
