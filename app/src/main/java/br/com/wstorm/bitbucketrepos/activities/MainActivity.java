package br.com.wstorm.bitbucketrepos.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.wstorm.bitbucketrepos.R;
import br.com.wstorm.bitbucketrepos.connection.ApiServices;
import br.com.wstorm.bitbucketrepos.model.ApiResponseRepos;
import br.com.wstorm.bitbucketrepos.model.Repository;
import br.com.wstorm.bitbucketrepos.util.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private ProgressBar progressBar;
    private List<Repository> repositories;
    private ReposAdapter adapter;

    private int page = 1;
    private int currentFirstVisibleItem;
    private int currentVisibleItemCount;
    private int currentScrollState;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.lvRepos);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        repositories = new ArrayList<>();
        adapter = new ReposAdapter(getApplication(), repositories);
        listView.setAdapter(adapter);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                currentScrollState = scrollState;
                isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                currentFirstVisibleItem = firstVisibleItem;
                currentVisibleItemCount = visibleItemCount;

            }
        });

        callService(page);

    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == 0) {

            if(!isLoading){
                isLoading = true;
                page++;
                callService(page);
            }
        }
    }

    private void callService(int page) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ApiServices service = retrofit.create(ApiServices.class);

        Call call = service.getRepos(String.format(Constants.URL_REPOS, page));
        call.enqueue(new Callback<ApiResponseRepos>() {

            @Override
            public void onResponse(Call<ApiResponseRepos> call, Response<ApiResponseRepos> response) {

                if (response.body() != null) {

                    repositories.addAll(response.body().getRepositories());
                    ((ReposAdapter)listView.getAdapter()).setContent(repositories);
                    ((ReposAdapter) listView.getAdapter()).notifyDataSetChanged();
                    listView.setVisibility(View.VISIBLE);

                }

                progressBar.setVisibility(View.GONE);
                isLoading = false;

            }

            @Override
            public void onFailure(Call<ApiResponseRepos> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }

        });

    }

    class ReposAdapter extends BaseAdapter {

        private static final int TYPE_NORMAL = 0;
        private static final int TYPE_PROGRESS = 1;

        private List<Repository> content;
        private Context context;

        public void setContent(List<Repository> content) {
            this.content = content;
        }

        public ReposAdapter(Context context, List<Repository> content) {

            this.context = context;
            this.content = content;
        }

        @Override
        public int getCount() {

            return content.size();

        }

        @Override
        public Object getItem(int i) {
            return content.get(i);
        }

        @Override
        public long getItemId(int i) {
            return content.get(i).getId();
        }

        @Override
        public int getItemViewType(int position) {

            if (position == (content.size())) {
                return TYPE_PROGRESS;
            }

            return TYPE_NORMAL;

        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {

            final Repository repository = content.get(position);

            int type = getItemViewType(position);

            View layout = view;

            if (layout == null) {
                if (type == TYPE_NORMAL) {
                    layout = LayoutInflater.from(context).inflate(R.layout.cell_repos, null);
                } else {
                    layout = LayoutInflater.from(context).inflate(R.layout.cell_progress, null);
                }
            }

            if (type == TYPE_NORMAL) {

                TextView txTitle = (TextView) layout.findViewById(R.id.txTitle);
                TextView txDetails = (TextView) layout.findViewById(R.id.txDetails);
                TextView txFork = (TextView) layout.findViewById(R.id.txFork);
                TextView txStars = (TextView) layout.findViewById(R.id.txStars);
                TextView txName = (TextView) layout.findViewById(R.id.txName);
                ImageView imPhoto = (ImageView) layout.findViewById(R.id.imPhoto);

                Picasso.with(context).
                        load(repository.getOwner().getAvatarUrl()).
                        placeholder(R.drawable.placeholder).
                        error(R.drawable.placeholder).into(imPhoto);

                txTitle.setText(repository.getName());
                txDetails.setText(repository.getDescription());
                txFork.setText(String.valueOf(repository.getForksCount()));
                txStars.setText(String.valueOf(repository.getStargazersCount()));
                txName.setText(repository.getOwner().getLogin().trim());

                layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, DetailsActivity.class);
                        intent.putExtra("owner", repository.getOwner().getLogin());
                        intent.putExtra("repository", repository.getName());
                        startActivity(intent);
                    }
                });

            }

            return layout;
        }

    }

}
