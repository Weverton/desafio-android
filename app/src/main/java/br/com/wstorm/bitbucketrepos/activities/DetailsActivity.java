package br.com.wstorm.bitbucketrepos.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import br.com.wstorm.bitbucketrepos.R;
import br.com.wstorm.bitbucketrepos.connection.ApiServices;
import br.com.wstorm.bitbucketrepos.model.PullRequest;
import br.com.wstorm.bitbucketrepos.util.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailsActivity extends AppCompatActivity {

    private ListView listView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle("Pull Requests");

        listView = (ListView) findViewById(R.id.lvDetails);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        String user = getIntent().getExtras().getString("owner");
        String repo = getIntent().getExtras().getString("repository");

        callService(user, repo);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    private void callService(String user, String repo) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ApiServices service = retrofit.create(ApiServices.class);

        Call call = service.getPullRequests(String.format(Constants.URL_DETAILS, user, repo));
        call.enqueue(new Callback<List<PullRequest>>() {


            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {

                if (response.body() != null) {

                    listView.setAdapter(new PullRequestAdapter(getApplication(), response.body()));
                    listView.setVisibility(View.VISIBLE);

                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                Log.e("DetailsActivity", t.getMessage());
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    class PullRequestAdapter extends BaseAdapter {

        private List<PullRequest> content;
        private Context context;


        public PullRequestAdapter(Context context, List<PullRequest> content) {

            this.context = context;
            this.content = content;
        }

        @Override
        public int getCount() {

            return content.size();

        }

        @Override
        public Object getItem(int i) {
            return content.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {

            final PullRequest pullRequest = content.get(position);

            View layout = view;

            if (layout == null) {
                layout = LayoutInflater.from(context).inflate(R.layout.cell_pull_request, null);
            }

            TextView txTitle = (TextView) layout.findViewById(R.id.txTitle);
            TextView txDetails = (TextView) layout.findViewById(R.id.txDetails);
            TextView txDate = (TextView) layout.findViewById(R.id.txDate);
            TextView txName = (TextView) layout.findViewById(R.id.txName);
            ImageView imPhoto = (ImageView) layout.findViewById(R.id.imPhoto);

            Picasso.with(context).
                    load(pullRequest.getUser().getAvatarUrl()).
                    placeholder(R.drawable.placeholder).
                    error(R.drawable.placeholder).into(imPhoto);

            try {

                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(pullRequest.getCreated_at());

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                //dateFormat.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
                txDate.setText(dateFormat.format(date));

            } catch (ParseException e) {
                e.printStackTrace();
            }

            txTitle.setText(pullRequest.getTitle());
            txDetails.setText(pullRequest.getBody());

            txName.setText(pullRequest.getUser().getLogin());

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(pullRequest.getHtml_url()));
                    startActivity(i);
                }
            });


            return layout;
        }

    }
}
