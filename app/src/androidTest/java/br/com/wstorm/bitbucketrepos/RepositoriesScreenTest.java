package br.com.wstorm.bitbucketrepos;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.wstorm.bitbucketrepos.activities.MainActivity;
import br.com.wstorm.bitbucketrepos.model.PullRequest;
import br.com.wstorm.bitbucketrepos.model.Repository;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class RepositoriesScreenTest  {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule(MainActivity.class);

    public void setUp() {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void chooseItem() {

        setUp();

        onView(allOf(withText("facebook"))).perform(click());

    }

    @Test
    public void scrollList() {

        setUp();

        onData(is(instanceOf(Repository.class)))
                .inAdapterView(allOf(withId(R.id.lvRepos), isDisplayed()))
                .atPosition(20).perform(click());

    }

}